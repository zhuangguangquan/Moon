# Moon应用地址下载说明

此为Moon应用地址下载页面，

提供两种方式下载:

方式1：
   下载链接: https://gitee.com/zhuangguangquan/Moon/raw/master/moon_weibo_v1.0.01_release_20190711135832_1001_jiagu_sign.apk

方式2：
   下载链接: https://pan.baidu.com/s/1tPWb5DwetuddFvpmxqFlhQ 提取码: 7qeq


针对审核反馈的情况，如下，审核说明太少了，我这里明明有应用的下载地址，请帮忙审核，多谢，已经提交多次了

你的应用“Moon微博客户端”申请驳回 
驳回理由：应用地址页面缺少对应的客户端下载

#### 项目介绍
微博第三方客户端数据，Moon微博客户端
